import numpy as np
import tensorflow as tf
import os
import pickle
import cv2
from scipy.spatial.distance import cdist
import matplotlib.pyplot as plt

ROOT_PATH = 'C:/Users/osipe/Pictures'
IMG_DICT_HIST = 'Images Dicts/img_dict_hist.pickle'
IMG_DICT_NET = 'Images Dicts/img_dict_net.pickle'

IN_V3 = tf.keras.applications.InceptionV3(
    include_top=False, weights='imagenet', input_tensor=None, input_shape=(299, 299, 3),
    pooling=None
)

last = IN_V3.output
x = tf.keras.layers.Flatten()(last)
model = tf.keras.Model(IN_V3.input, x)


def get_out_net(img):
    resized_img = cv2.resize(img, (299, 299), interpolation=cv2.INTER_AREA)
    out = model.predict(resized_img.reshape(-1, 299, 299, 3))[0]
    return out


def get_img_hist(img, n_bins=50):
    r_hist, _ = np.histogram(img[:, :, 0][0], bins=n_bins)
    g_hist, _ = np.histogram(img[:, :, 1][0], bins=n_bins)
    b_hist, _ = np.histogram(img[:, :, 2][0], bins=n_bins)
    hist_ = np.concatenate((r_hist, g_hist, b_hist))
    hist_ = hist_ / hist_.sum()
    return hist_


def find_all_images(root_path, use_net):
    img_dict = {}
    for root, _, files in os.walk(root_path):
        for file in files:
            if file.endswith(('.jpg', '.png')):
                path_ = os.path.normpath(os.path.join(root, file))
                img = cv2.imread(path_)
                if img is not None:
                    if use_net:
                        img_dict[path_] = get_out_net(img)
                    else:
                        img_dict[path_] = get_img_hist(img)
    return img_dict


def find_similar_images(img_path, img_dict, n, use_net, metric='euclidean'):
    img_path = os.path.normpath(img_path)
    base_img = cv2.imread(img_path, cv2.IMREAD_COLOR)
    if img_path in img_dict:
        base_img_hist = img_dict[img_path].reshape((1, -1))
    else:
        if base_img is not None:
            if use_net:
                base_img_hist = get_out_net(base_img).reshape((1, -1))
            else:
                base_img_hist = get_img_hist(base_img).reshape((1, -1))
        else:
            print('No such image or it\'s not BRG.')
            return

    m_sim = cdist(base_img_hist, np.array(list(img_dict.values())), metric=metric)
    temp = list(zip(list(img_dict.keys()), m_sim[0]))
    temp.sort(key=lambda x: x[1])

    plt.imshow(cv2.cvtColor(base_img, cv2.COLOR_BGR2RGB))
    plt.title('Base Image')
    plt.show()

    for x in temp[:n]:
        img = cv2.imread(x[0])
        plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
        plt.title(f'Similarity {x[1]}')
        plt.show()


def main(img_path, n=20, use_net=False):
    dict_name = IMG_DICT_HIST
    if use_net:
        dict_name = IMG_DICT_NET
    if os.path.isfile(dict_name):
        with open(dict_name, 'rb') as file:
            img_dict = pickle.load(file)
    else:
        img_dict = find_all_images(ROOT_PATH, use_net)
        with open(dict_name, 'wb') as file:
            pickle.dump(img_dict, file, protocol=pickle.HIGHEST_PROTOCOL)

    find_similar_images(img_path, img_dict, n, use_net, metric='minkowski')


if __name__ == '__main__':
    print('Enter path to image:')
    img_path = input()
    main(img_path, n=5, use_net=True)
